﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace JimmyErickHuamaniMendozaExamen
{
    public class bd_connection
    {

        SqlConnection sql_connection;

        public bool ConnectServer()
        {
            try
            {
                String server_connection = @"Server = DESKTOP-0KLD8KD\SQLEXPRESS ; DataBase = DBOdontologico; Integrated Security = SSPI";
                sql_connection = new SqlConnection(server_connection);
                sql_connection.Open();
                return true;

            } catch
            {
                return false;
            }
        }

        public void DisconnetServer()
        {
            sql_connection.Close();
        }

        public DataSet Query_execution(String string_query)
        {
            DataSet ds = new DataSet();
            if (ConnectServer())
            {
                try
                {
                    SqlDataAdapter da = new SqlDataAdapter(string_query, sql_connection);
                    da.Fill(ds);
                }
                catch (Exception ex)
                {                }
            }
            return ds;
        }

        public int Execute_command(String string_query)
        {
            int res = -1;
            if (ConnectServer())
            {
                try
                {
                    SqlCommand cmd = new SqlCommand(string_query, sql_connection);
                    cmd.ExecuteNonQuery();
                    res = 0;
                } catch
                {

                }
            }
            return res;
        }
    }
}
